package base;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import pages.MainPage;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Selenide.close;

public class BaseTestForGrid {

    public MainPage openApp(String device) {
        WebDriver driver = null;
        try {
            driver = getAndroidDriver(device);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            System.out.println("We have problems with URL for driver");
        }
        WebDriverRunner.setWebDriver(driver);

        return new MainPage();
    }

    public static AndroidDriver getAndroidDriver(String device) throws MalformedURLException {

        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("platformName", "Android");

        switch (device) {
            case "Pixel 5 Android S":
                capabilities.setCapability("udid", "emulator-5554");
                break;
            case "Pixel 3a":
                capabilities.setCapability("udid", "emulator-5556");
        }

        capabilities.setCapability("app", "C:\\Users\\zverev\\Downloads\\Android-NativeDemoApp-0.2.1.apk");

        Configuration.reportsFolder = "screenshots/actual";

        return new AndroidDriver(new URL("http:/127.0.0.1:4444/wd/hub"), capabilities);

    }

    @AfterClass
    public void setDown() {
        close();
    }
}
