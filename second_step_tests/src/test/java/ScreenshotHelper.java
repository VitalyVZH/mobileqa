import base.BaseTest;
import io.appium.java_client.MobileBy;
import org.testng.annotations.Test;

import java.io.File;

import static com.codeborne.selenide.Selenide.$;

public class ScreenshotHelper extends BaseTest {

    @Test
    public void createScreenshot() {
        openApp()
                .clickFormsMenuButton();
        File actualScreenShot = $(MobileBy.AccessibilityId("Forms-screen")).screenshot();
    }
}
