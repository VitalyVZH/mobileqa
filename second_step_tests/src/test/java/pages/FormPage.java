package pages;

import com.codeborne.selenide.Condition;
import com.github.romankh3.image.comparison.ImageComparison;
import com.github.romankh3.image.comparison.ImageComparisonUtil;
import com.github.romankh3.image.comparison.model.ImageComparisonResult;
import com.github.romankh3.image.comparison.model.ImageComparisonState;
import io.qameta.allure.Step;
import locators.FormPageLocators;

import java.awt.image.BufferedImage;
import java.io.File;

import static com.codeborne.selenide.Selenide.$;
import static org.testng.Assert.assertEquals;

public class FormPage {

    private String expectedText = "Test text";

    private FormPageLocators locator() {
        return new FormPageLocators();
    }

    @Step("Заполение поля ввода текстом")
    public FormPage fillTextField() {
        $(locator().formInputField()).sendKeys(expectedText);
        return new FormPage();
    }

    @Step("Проверка формы вывода на ошибки")
    public FormPage checkOutputTextField(String text) {
        $(locator().formInputResultField()).shouldHave(Condition.text(text));
        return new FormPage();
    }

    @Step("Нажатие на переключатель")
    public FormPage clickSwitcher() {
        $(locator().switcher()).click();
        return new FormPage();
    }

    @Step("Проверка формы вывода на ошибки у переключателя")
    public FormPage checkOutputTextSwitcher(String text) {
        $(locator().switcherText()).shouldHave(Condition.text(text));
        return new FormPage();
    }

    @Step("Скриншот страницы Формы для сравния с требованиями")
    public FormPage checkScreenshot() {
        BufferedImage expectedImage = ImageComparisonUtil.readImageFromResources("src/main/resources/expectedScreenshots/formsPage.png");
        File actualScreenshot = $(locator().formsScreen()).screenshot();
        BufferedImage actualImage = ImageComparisonUtil.readImageFromResources("screenshots/actual/" + actualScreenshot.getName());
        File resultDestination = new File("diff/diff_CheckFailMainPageScreenshot.png");

        ImageComparisonResult imageComparisonResult = new ImageComparison(expectedImage, actualImage, resultDestination).compareImages();
        assertEquals(ImageComparisonState.MATCH, imageComparisonResult.getImageComparisonState());

        return this;
    }
}
