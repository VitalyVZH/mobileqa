package pages;

import com.github.romankh3.image.comparison.ImageComparison;
import com.github.romankh3.image.comparison.ImageComparisonUtil;
import com.github.romankh3.image.comparison.model.ImageComparisonResult;
import com.github.romankh3.image.comparison.model.ImageComparisonState;
import io.qameta.allure.Step;
import locators.MainPageLocators;

import java.awt.image.BufferedImage;
import java.io.File;

import static com.codeborne.selenide.Selenide.$;
import static org.testng.Assert.assertEquals;

public class MainPage {

    private MainPageLocators locator() {
        return new MainPageLocators();
    }

    @Step("Клик на кнопке Login и переход на страницу логина")
    public LoginPage clickLoginMenuButton() {
        $(locator().loginButton()).click();
        return new LoginPage();
    }

    @Step("Клик по кнопке Forms и переход на страницу Формы")
    public FormPage clickFormsMenuButton() {
        $(locator().formsButton()).click();
        return new FormPage();
    }

    @Step("Скриншот главной страницы для сравния с требованиями")
    public MainPage checkScreenshot() {
        BufferedImage expectedImage = ImageComparisonUtil.readImageFromResources("src/main/resources/expectedScreenshots/mainPage.png");
        File actualScreenshot = $(locator().homeScreen()).screenshot();
        BufferedImage actualImage = ImageComparisonUtil.readImageFromResources("screenshots/actual/" + actualScreenshot.getName());
        File resultDestination = new File("diff/diff_CheckFailMainPageScreenshot.png");

        ImageComparisonResult imageComparisonResult = new ImageComparison(expectedImage, actualImage, resultDestination).compareImages();
        assertEquals(ImageComparisonState.MATCH, imageComparisonResult.getImageComparisonState());

        return this;
    }
}
