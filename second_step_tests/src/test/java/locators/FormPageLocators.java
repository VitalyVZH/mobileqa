package locators;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class FormPageLocators {

    public By formInputField() {
        return MobileBy.AccessibilityId("text-input");
    }

    public By formInputResultField() {
        return MobileBy.AccessibilityId("input-text-result");
    }

    public By switcher() {
        return MobileBy.AccessibilityId("switch");
    }

    public By switcherText() {
        return MobileBy.AccessibilityId("switch-text");
    }

    public By formsScreen() {
        return MobileBy.AccessibilityId("Forms-screen");
    }
}
