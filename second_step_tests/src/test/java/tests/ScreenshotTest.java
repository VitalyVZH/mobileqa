package tests;

import base.BaseTest;
import org.testng.annotations.Test;

public class ScreenshotTest extends BaseTest {

    @Test
    public void checkMainPageScreenshot() {
        openApp()
                .checkScreenshot();
    }

    @Test
    public void checkFormsPageScreenshot() {
        openApp()
                .clickFormsMenuButton()
                .checkScreenshot();
    }
}
