package tests;

import base.BaseTest;
import org.testng.annotations.Test;

public class FormTest extends BaseTest {

    @Test
    public void checkInputField() {

        openApp()
                .clickFormsMenuButton()
                .fillTextField()
                .checkOutputTextField("Test text");
    }

    @Test
    public void checkSwitchOff() {

        openApp()
                .clickFormsMenuButton()
                .clickSwitcher()
                .checkOutputTextSwitcher("Click to turn the switch OFF");
    }

    @Test
    public void checkSwitchOn() {

        openApp()
                .clickFormsMenuButton()
                .clickSwitcher()
                .checkOutputTextSwitcher("Click to turn the switch OFF")
                .clickSwitcher()
                .checkOutputTextSwitcher("Click to turn the switch ON");
    }
}
