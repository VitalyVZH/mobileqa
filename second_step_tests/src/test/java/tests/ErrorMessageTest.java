package tests;

import base.BaseTest;
import io.qameta.allure.Description;

import listeners.AllureListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(AllureListener.class)
public class ErrorMessageTest extends BaseTest {

    @Test
    @Description("Проверка сообщения об ошибку при невалидном email")
    public void checkInputField() {

        openApp()
                .clickLoginMenuButton()
                .clickLoginButton()
                .checkLoginErrorText("Please enter a valid email address");
    }
}
