package tests;

import base.BaseTestForGrid;
import io.qameta.allure.Description;
import listeners.AllureListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(AllureListener.class)
public class ErrorMessageForGridTest extends BaseTestForGrid {

    @Test
    @Description("Проверка сообщения об ошибку при невалидном email")
    public void checkInputFieldFirst() {

        openApp("Pixel 5 Android S")
                .clickLoginMenuButton()
                .clickLoginButton()
                .checkLoginErrorText("Please enter a valid email address");
    }

    @Test
    @Description("Проверка сообщения об ошибку при невалидном email")
    public void checkInputFieldSecond() {

        openApp("Pixel 3a")
                .clickLoginMenuButton()
                .clickLoginButton()
                .checkLoginErrorText("Please enter a valid email address");
    }
}
