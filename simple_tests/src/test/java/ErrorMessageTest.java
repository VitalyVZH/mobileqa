import io.appium.java_client.MobileElement;
import org.testng.Assert;
import org.testng.annotations.*;

public class ErrorMessageTest extends BaseTest {

    @Test(dataProvider = "dataLogin")
    public void checkEmptyFieldsLogin(String xpath, String error) {

        ((MobileElement) driver.findElementByAccessibilityId("Login")).click();
        ((MobileElement) driver.findElementByAccessibilityId("button-LOGIN")).click();
        String errorText = ((MobileElement)
                driver.findElementByXPath(xpath)).getText();

        Assert.assertEquals(errorText, error);
    }

    @Test(dataProvider = "dataSignUp")
    public void checkEmptyFieldsSignUp(String xpath, String error) {

        ((MobileElement) driver.findElementByAccessibilityId("Login")).click();
        ((MobileElement) driver.findElementByAccessibilityId("button-sign-up-container")).click();
        ((MobileElement) driver.findElementByAccessibilityId("button-SIGN UP")).click();
        ((MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button")).click();

        String errorText = ((MobileElement)
                driver.findElementByXPath(xpath)).getText();


        Assert.assertEquals(errorText, error);
    }

    @DataProvider(name = "dataLogin")
    private Object[][] dataLogin() {
        return new Object[][] {
                {"//android.widget.ScrollView[@content-desc='Login-screen']/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView[1]", "Please enter a valid email address"},
                {"//android.widget.ScrollView[@content-desc='Login-screen']/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView[2]", "Please enter at least 8 characters"}
        };
    }

    @DataProvider(name = "dataSignUp")
    private Object[][] dataSignUp() {
        return new Object[][] {
                {"//android.widget.ScrollView[@content-desc=\"Login-screen\"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView[1]", "Please enter a valid email address"},
                {"//android.widget.ScrollView[@content-desc=\"Login-screen\"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView[2]", "Please enter at least 8 characters"},
                {"//android.widget.ScrollView[@content-desc=\"Login-screen\"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView[3]", "Please enter the same password"}
        };
    }
}
