import io.appium.java_client.MobileElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FormsTest extends BaseTest {

    @Test
    public void checkInputField() {

        String expectedText = "Test text";

        ((MobileElement) driver.findElementByAccessibilityId("Forms")).click();
        ((MobileElement) driver.findElementByAccessibilityId("text-input")).sendKeys(expectedText);
        String actualText = ((MobileElement)
                driver.findElementByAccessibilityId("input-text-result")).getText();

        Assert.assertEquals(actualText, expectedText);
    }

    @Test
    public void checkSwitchOff() {

        String expectedText = "Click to turn the switch OFF";

        ((MobileElement) driver.findElementByAccessibilityId("Forms")).click();
        ((MobileElement) driver.findElementByAccessibilityId("switch")).click();
        String actualText = ((MobileElement)
                driver.findElementByAccessibilityId("switch-text")).getText();

        Assert.assertEquals(actualText, expectedText);
    }

    @Test
    public void checkSwitchOn() {

        String expectedTextOff = "Click to turn the switch OFF";
        String expectedTextOn = "Click to turn the switch ON";

        ((MobileElement) driver.findElementByAccessibilityId("Forms")).click();
        ((MobileElement) driver.findElementByAccessibilityId("switch")).click();
        String actualTextOff = ((MobileElement)
                driver.findElementByAccessibilityId("switch-text")).getText();

        Assert.assertEquals(actualTextOff, expectedTextOff);

        ((MobileElement) driver.findElementByAccessibilityId("switch")).click();
        String actualTextOn = ((MobileElement)
                driver.findElementByAccessibilityId("switch-text")).getText();

        Assert.assertEquals(actualTextOn, expectedTextOn);
    }
}
